# Separate Linux audio input in OBS

This is a guide on how to separate audio input in OBS-Studio, resulting in separate audio streams on the resulting video file for each audio input.

If you have any suggestions or fixes for this guide feel free to open an issue!

![Kdenlive Demonstration](https://gitlab.com/zerowhy/obs-input-guide/-/raw/main/kdenlive.png)\
_Separate audio channels in Kdenlive_

## Prerequisites

For this to work, you will have to be using pipewire with JACK support installed for your audio needs. I have no idea if this is possible on Pulseaudio.

On Arch Linux, JACK support on Pipewire is provided by the `pipewire-jack` package, available on the `extra` repository.\
Another program is necessary: [qpwgraph](https://gitlab.freedesktop.org/rncbc/qpwgraph).
On Arch Linux, it is available in the `qpwgraph` package, in the `community` repository.

## OBS configuration

To start, you will have to setup some things in OBS. You will be creating all the audio inputs/sources that you desire!

Open up OBS and click to add a source on the audio sources list. Select "JACK Input Client". Select "Create New", type in a name to your input (like "Discord" or "Game Audio") and click OK. Now a window will popup asking you for the number of channels: if your audio source is stereo leave it at 2, if not change it accordingly. Leave the "Start JACK Server" off and press OK.

Each source you add can be a separate application, so create as many as you wish.

To leave the audio separated in the final video file, you have configure audio tracks in OBS.\
First go to the settings and into the "Output" tab. Go to Recording and enable as many tracks as you want in "Audio Tracks". Each one you enable will be a separate audio track on your video editor.

To connect the audio tracks to the audio sources, go to the "Audio Mixer" (by default on the bottom of the app) and right click and open "Advanced Audio Properties". In the popup open, you can enable/disable the inputs on the tracks to fit your needs. 

## qpwgraph configuration

Now, with OBS setup correctly, open qpwgraph.

Probably it will be a complete mess, with a bunch of lines, but stay calm, it is very simple.

In this application you can route audio via the GUI by clicking and draging from inputs/outputs and joining them together. If everything went well, you will see your OBS inputs with the names you created on the app and the audio output from different apps on your system. The only thing you have to do is join up the output from an app into the input from OBS, like in this image:

![qpwgraph setup example](https://gitlab.com/zerowhy/obs-input-guide/-/raw/main/qpwgraph.png)

Some applications have strange names in their audio outputs, like Discord (which is "WebRTC VoiceEngine"). The only way to find out which app is which output is by testing and listening to it. You can test that in an app to control audio like [pavucontrol](https://freedesktop.org/software/pulseaudio/pavucontrol/) (works on Pipewire with pipewire-pulse)

If you are using 2 channel audio (stereo), make sure to join up both the outputs to their respective inputs from OBS as seen in the image.

If the application that is outputting audio stops its audio output for some reason, you connection between its output and OBS might be disconnected, so watch out for it! (when leaving a call in Discord it disconnects, for example)

I also recommend saving the configuration on qpwgraph with the "Save" option under "Patchbay" on the app.

## Conclusion

Thanks for reading this guide and I hope this helps you out! If you have any suggestions feel free to leave them at the issues tab here on GitLab.
